from dremio.flight.endpoint import DremioFlightEndpoint
from argparse import Namespace
import certifi
from abc import ABC
from decouple import config

class DremioAbstract(ABC):
   
    def execute_query(self, query):
        
        args_dict = {
            "hostname": config('hostname'),
            "port": config('port'),
            "username": config('username'),
            "password": config('password'),
            "query": query,
            "token": None,
            "tls": False,
            "disable_certificate_verification": True,
            "path_to_certs": certifi.where(),
            "session_properties": None,
            "engine": None,
        }

        args_namespace = Namespace(**args_dict)
        # Instantiate DremioFlightEndpoint object
        dremio_flight_endpoint = DremioFlightEndpoint(args_namespace)

        # Connect to Dremio Arrow Flight server endpoint.
        flight_client = dremio_flight_endpoint.connect()

        # Execute query
        return dremio_flight_endpoint.execute_query(flight_client)





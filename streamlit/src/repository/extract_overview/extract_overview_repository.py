from repository.dremio_abstract import DremioAbstract

""" Repository with query to provide overview about data extract from the applciations """
class ExtractOverViewRepository(DremioAbstract):
    
    def __init__(self) -> None:
        self.ciro_  act_query = 'SELECT * FROM "the_band"."extract"."ciro_extract_artifact"'
        self.ciro_extract_project_query = 'SELECT * FROM "the_band"."extract"."ciro_extract_project"'
    
        self.sro_extract_user_story_project_query = 'SELECT * FROM "the_band"."extract"."sro_extract_user_story_project"'
    
    def ciro_extract_artifact(self):
        """ Quantity of artifact saved in CIRO"""
        return super().execute_query(self.ciro_extract_artifact_query)

    def ciro_extract_project(self):
        """ Quantity of project saved in CIRO """
        return super().execute_query(self.ciro_extract_project_query)
    
    def sro_extract_user_story_project(self):
        """ Quantity of user story saved in SRO """
        return super().execute_query(self.sro_extract_user_story_project_query)
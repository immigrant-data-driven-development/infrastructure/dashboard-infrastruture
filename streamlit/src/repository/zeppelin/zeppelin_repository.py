from repository.dremio_abstract import DremioAbstract

""" Repository with query to provide data for Zeppelin`s Assertives """
class ZeppelinRepository(DremioAbstract):
    
    def __init__(self) -> None:
    
        self.zeppelin_user_story_no_effort = 'SELECT * FROM "the_band".zeppelin."zeppelin_sro_quantity_user_story_no_effort"'
        
        self.zeppelin_sro_ciro_quantity_project_repository_branch = 'SELECT * FROM "the_band".zeppelin."zeppelin_sro_ciro_quantity_project_repository_branch"'
        
        self.zeppelin_sro_ciro_quantity_project_repository_branch_merged = 'SELECT * FROM "the_band".zeppelin."sro_ciro_quantity_project_repository_branch_merged"'
        
        self.zeppelin_quantity_sprint_project = 'SELECT * FROM "the_band".zeppelin."zeppelin_quantity_sprint_project"'
        
        self.zeppelin_sprint_max_start_end_date = 'SELECT * FROM "the_band".zeppelin."zeppelin_sprint_max_start_end_date"'
        
        self.zeppelin_quantity_stakeholders_project = 'SELECT * FROM "the_band".zeppelin."zeppelin_quantity_stakeholders_project"'

        self.zeppelin_backend_developer = 'SELECT * FROM "the_band".zeppelin."zeppelin_develeper_backend"'

        self.zeppelin_frontend_developer = 'SELECT * FROM "the_band".zeppelin."zeppelin_develeper_frontend"'

        self.zeppelin_scrum_master = 'SELECT * FROM "the_band".zeppelin."zeppelin_scrummaster_product_onwer"'

        self.zeppelin_user_story_by_date = 'SELECT * FROM "the_band".zeppelin."zeppelin_user_story_by_date"'

    
    def user_story_no_effort(self):
        """ Retrive all User Stories without Story points"""
        return super().execute_query(self.zeppelin_user_story_no_effort)

    def user_story_by_date(self):
        """ Retrive all User Stories create by creation date and project """
        return super().execute_query(self.zeppelin_user_story_by_date)
    
    def quantity_project_repository_branch(self):
        """ Retrive quantity of project`s repository and branches """
        return super().execute_query(self.zeppelin_sro_ciro_quantity_project_repository_branch)

    def quantity_project_repository_branch_merged(self):
        """ Retrive quantity of project`s repository and merged and not merged branches """
        return super().execute_query(self.zeppelin_sro_ciro_quantity_project_repository_branch_merged)

    def sprint_max_start_end_date(self):
        """ Retrive Project`s sprints """
        return super().execute_query(self.zeppelin_sprint_max_start_end_date)
    
    def quantity_sprint_project(self):
        """ Retrive Quantity of Project`s sprints """
        return super().execute_query(self.zeppelin_quantity_sprint_project)

    def quantity_stakeholders_project(self):
        """ Retrive the Project`s stakeholder """
        return super().execute_query(self.zeppelin_quantity_stakeholders_project)

    def backend_developer(self):
        """ Retrive the Project`s backend """
        return super().execute_query(self.zeppelin_backend_developer)

    def frontend_developer(self):
        """ Retrive the Project`s frontend """
        return super().execute_query(self.zeppelin_frontend_developer)

    def scrum_master(self):
        """ Retrive the Project`s scrummaster """
        return super().execute_query(self.zeppelin_scrum_master)
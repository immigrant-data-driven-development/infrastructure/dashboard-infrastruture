from repository.dremio_abstract import DremioAbstract

class TheBandRepository(DremioAbstract):
    
    def __init__(self) -> None:
        self.the_band_growth_rate_project_view = 'SELECT * FROM theband.sro."growth_rate_project_view"'
        self.the_band_product_backlog = 'SELECT project, userstory, userstorytype FROM theband.sro."product_backlog_view"'
        self.the_band_waiting_time_view = 'SELECT ROUND(average_waiting_day,2) average_waiting_day, project FROM theband.sro."waiting_time_view"'    
        self.the_band_growth_rate_product_backlog = 'SELECT * FROM theband.sro."growth_rate_product_backlog_project_view"'
        self.the_band_growth_rate_product_backlog_acummulate = 'SELECT project, event_date, accumulated_sum FROM theband.sro."growth_rate_product_backlog_project_accummulate_view"'
        self.the_band_project_start_end_date = 'SELECT * FROM theband.sro."project_start_end_date_view"'
    
    def project_start_end_date(self):
        return super().execute_query(self.the_band_project_start_end_date)
    
    def growth_rate_project(self):
        return super().execute_query(self.the_band_growth_rate_project_view)

    def product_backlog(self):
        return super().execute_query(self.the_band_product_backlog)
    
    def waiting_time(self):
        return super().execute_query(self.the_band_waiting_time_view)
    
    def growth_rate_product_backlog(self):
        return super().execute_query(self.the_band_growth_rate_product_backlog)

    def growth_rate_product_backlog_acummulate(self):
        return super().execute_query(self.the_band_growth_rate_product_backlog_acummulate)
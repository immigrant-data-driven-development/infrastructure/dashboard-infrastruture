from st_pages import show_pages_from_config, add_page_title
import streamlit as st
from controllers.theband_overview_controller import TheBandOverviewController

add_page_title()
show_pages_from_config()
theBandOverviewController = TheBandOverviewController()
DP, PP, SO, AQ, PQ= st.tabs(["🏠 Development Process", "🗃 Planning Project",  " 📈 Scopes", " 🎈️ Artifact Quality", " 🎈️ Process Quality", ])


with DP:
   theBandOverviewController.development_process()
   
with PP:
   theBandOverviewController.planning_project()

with SO:
   theBandOverviewController.scopes()

with AQ:
   theBandOverviewController.quality_artifact()

with PQ:
   theBandOverviewController.quality_process()

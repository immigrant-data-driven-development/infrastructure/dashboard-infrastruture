from st_pages import show_pages_from_config, add_page_title
import streamlit as st

from controllers.zeppelin_overview_controller import ZeppelinOverviewController
add_page_title()
show_pages_from_config()
st.write ("Flying over an Organization with The Band")

AO, CI, CD, SI = st.tabs(["🏠 Agile Organization ", "🗃 Continuous Integration", " 🎈️ Continuous Deployment", " 📈 R&D as Innovation System"])

zeppelinOverviewController = ZeppelinOverviewController ()

with AO:
   
   zeppelinOverviewController.roles_in_projects()
   
   zeppelinOverviewController.project_scope()
   
   zeppelinOverviewController.project_user_story_no_effort()
   
   zeppelinOverviewController.sprint_overview()

   zeppelinOverviewController.quantity_stakeholders_project()
   
with CI:
   df = zeppelinOverviewController.quantity_project_repository_branch()
   
   df = zeppelinOverviewController.quantity_project_repository_branch_merged()
   

with CD:
   st.write("Under Development")

with SI:
   st.write("Under Development")

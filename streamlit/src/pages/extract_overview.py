from st_pages import show_pages_from_config, add_page_title
import pandas as pd
import streamlit as st
import seaborn as sns
import matplotlib.pyplot as plt

from controllers.extract_overview_controller import ExtractOverviewController

add_page_title()
show_pages_from_config()

extractOverviewController = ExtractOverviewController()
project_tab, artifact_type_tab = st.tabs(["📈 Projects", "🗃 Artifact Types"])

with project_tab:
    st.write ("# CIRO Overview")
    df = extractOverviewController.ciro_extract_project()
    st.dataframe(df, use_container_width=True)
    
    st.write ("# SRO Overview")
    df = extractOverviewController.sro_extract_user_story_project()
    st.dataframe(df, use_container_width=True)
    
with artifact_type_tab:
    
    st.write ("# CIRO Overview")
    df = extractOverviewController.ciro_extract_artifact()
    st.dataframe(df, use_container_width=True)


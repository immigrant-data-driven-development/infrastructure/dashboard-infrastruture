import pandas as pd
import streamlit as st
import seaborn as sns
import matplotlib.pyplot as plt

from repository.zeppelin.zeppelin_repository import ZeppelinRepository
""" Zeppelin Page`s Controller  """
class ZeppelinOverviewController():
    
    def __init__(self):
        self.repository = ZeppelinRepository()

    def project_scope(self):
        """Answer the Zeppelin`s Assertive: [AO.03] The scope of the project is defined gradually, using the Product Backlog (or equivalent artifact)."""
        st.write("[AO.03] The scope of the project is defined gradually, using the Product Backlog (or equivalent artifact).")
        
        df = self.repository.user_story_by_date()
        
        df['created_date'] = pd.to_datetime(df['created_date'])
        
        sns.set_theme(style="darkgrid")
        
        df.rename(columns={'userstorytype':'User Story Type','created_date': 'Created Date','project_name': 'Project','quantity': 'EPIC/User Story'}, inplace=True)
        
        sns.lineplot(x="Created Date", y="EPIC/User Story",
             hue="Project", style="User Story Type",
             data=df)
        
        plt.xticks(rotation=90)
        st.pyplot(plt)
        
        st.dataframe(df, use_container_width=True)
        

    def __display_developer_backend(self):
        st.write("Plays Developer Backend Role")
        
        df = self.repository.backend_developer()
        df.rename(columns={'project_name': 'Project Name','developer': "Developer"}, inplace=True)
        st.dataframe(df, use_container_width=True)

    def __display_developer_frontend(self):
        st.write("Plays Developer Front Role")
        df = self.repository.frontend_developer()

        df.rename(columns={'project_name': 'Project Name','developer': "Developer"}, inplace=True)
        st.dataframe(df, use_container_width=True)

    def __display_scrum_master(self):
        st.write("Plays Scrum Master\Product Owner Role")
        df = self.repository.scrum_master()

        df.rename(columns={'project_name': 'Project Name','stakeholder_name': "Scrum Master/Product Owner"}, inplace=True)
        st.dataframe(df, use_container_width=True)

    
    def roles_in_projects(self):
        """Answer the Zeppelin`s Assertive: [AO.01] Roles involved in the agile development process (e.g., Scrum Master, Product Owner, Developer, Tester) exist in the organization."""
        st.write("[AO.01] Roles involved in the agile development process (e.g., Scrum Master, Product Owner, Developer, Tester) exist in the organization.")
        
        self.__display_developer_backend()
        self.__display_developer_frontend()
        self.__display_scrum_master()

    def quantity_stakeholders_project(self):
        """Answer the Zeppelin`s Assertive: [AO.14] Teams are small (usually between 4 to 6 developers), self-organized and multidisciplinary."""
        st.write("[AO.14] Teams are small (usually between 4 to 6 developers), self-organized and multidisciplinary.")
        
        df = self.repository.quantity_stakeholders_project()
        
        df.rename(columns={'project_name': 'Project Name','quantity': "Stakeholders"}, inplace=True)
        
        st.dataframe(df, use_container_width=True)
        
        return df
    
    def sprint_overview(self):
        """ Answer the Zeppelin`s Assertive: [AO.07] The development process is performed iteratively, in short cycles (e.g., 2 weeks), in which selected project requirements recorded in a Sprint Backlog (or equivalent artifact) are developed. """
        st.write("[AO.07] The development process is performed iteratively, in short cycles (e.g., 2 weeks), in which selected project requirements recorded in a Sprint Backlog (or equivalent artifact) are developed.")
        
        self.quantity_sprint_project()
        
        self.sprint_max_start_end_date()
    
    def sprint_max_start_end_date(self):
        
        df = self.repository.sprint_max_start_end_date()
        
        st.write("Last Sprint by Project")
        
        df.rename(columns={'name': 'Project Name','max_start_date':'Sprints: Start Date','max_end_date':'Sprints: End Date'}, inplace=True)
        
        st.dataframe(df, use_container_width=True)
        
        return df

    def quantity_sprint_project(self):
        
        df = self.repository.quantity_sprint_project()
        
        st.write("Quantity of Sprints by Projects")
        
        df.rename(columns={'name': 'Project Name','quantity':'Sprints'}, inplace=True)
        
        st.dataframe(df, use_container_width=True)

        return df
    
    def project_user_story_no_effort(self):
        """ Answer the Zeppelin`s Assertive: [AO.04] Effort estimation is performed by (or together with) the development team considering short activities to implement a set of selected requirements (and not the project as a whole). """
        st.write("[AO.04] Effort estimation is performed by (or together with) the development team considering short activities to implement a set of selected requirements (and not the project as a whole).")
        
        df = self.repository.user_story_no_effort()
        
        df.rename(columns={'project_name': 'Project Name','quantity':'User Story Without Story Points'}, inplace=True)
        
        st.dataframe(df, use_container_width=True)
        
        return df

    def quantity_project_repository_branch(self):
        """ Answer the Zeppelin`s Assertive: [CI.12] Check-in good practices are applied in the development trunk (e.g., use of tools such as GitFlow and Toogle Feature). """
        st.write("[CI.12] Check-in good practices are applied in the development trunk (e.g., use of tools such as GitFlow and Toogle Feature).")
        
        st.write("Projects x Source Repository x Branch.")
        df = self.repository.quantity_project_repository_branch()
        sns.set_theme(style="whitegrid")

        g = sns.catplot(
            data=df, kind="bar",
            x="sourcerepository_name", y="quantity", hue="project_name",
            errorbar="sd", palette="dark", alpha=.6, height=6
        )
        
        g.despine(left=True)
        g.set_axis_labels("", "Branch")
        g.legend.set_title("Project")
        plt.xticks(rotation=90)
        st.pyplot(plt)
    
        df.rename(columns={'project_name': 'Project','sourcerepository_name': 'Repository','quantity':'Branch'}, inplace=True)
        st.dataframe(df, use_container_width=True)
        
        return df
    
    def quantity_project_repository_branch_merged(self):
        st.write("Projects x Source Repository x Branch x Merged.")
        df = self.repository.quantity_project_repository_branch_merged()

        sns.set_theme(style="whitegrid")

        g = sns.catplot (
            data=df, kind="bar",
            x="project_name",
            y="quantity", 
            hue="merged", 
            errorbar="sd", palette="dark", alpha=.6, height=6
        )
        
        g.despine(left=True)
        g.set_axis_labels("", "Branch")
        g.legend.set_title("Merged?")
        plt.xticks(rotation=90)
        st.pyplot(plt)
    
        df.rename(columns={'project_name': 'Project','sourcerepository_name': 'Repository','quantity':'Branch','merged':'Merged'}, inplace=True)
        st.dataframe(df, use_container_width=True)
        return df


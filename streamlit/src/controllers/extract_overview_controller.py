import pandas as pd
import streamlit as st
import seaborn as sns
import matplotlib.pyplot as plt

from repository.extract_overview.extract_overview_repository import ExtractOverViewRepository
""" Extract Page`s Controller  """
class ExtractOverviewController():

    def __init__(self) -> None:
        self.extractOverViewRepository = ExtractOverViewRepository()

    def sro_extract_user_story_project(self):
        df = self.extractOverViewRepository.sro_extract_user_story_project()

        g = sns.catplot(
            data=df, kind="bar",
            x="project_name", y="quantity",
            errorbar="sd", palette="dark", alpha=.6, height=6
        )
        g.despine(left=True)
        g.set_axis_labels("", "User Story without Story Points")
        g.legend.set_title("Project Name")
        plt.xticks(rotation=90)
        st.pyplot(plt)
        

        df.rename(columns={'project_name': 'Project Name','quantity':'US without Story Point'}, inplace=True)
        return df
    
    def ciro_extract_project(self):
        
        df = self.extractOverViewRepository.ciro_extract_project()
        df['extract_date'] = pd.to_datetime(df['extract_date'])

        g = sns.catplot(
            data=df, kind="bar",
            x="extract_date", y="quantity",
            errorbar="sd", palette="dark", alpha=.6, height=6
        )
        g.despine(left=True)
        g.set_axis_labels("", "Source Repository")
        g.legend.set_title("Extract Date")
        st.pyplot(plt)
        
        df.rename(columns={'extract_date': 'Date Extract','quantity':'Source Repository'}, inplace=True)
        plt.xticks(rotation=90)
        return df

    def ciro_extract_artifact(self):
        df = self.extractOverViewRepository.ciro_extract_artifact()

        df['extract_date'] = pd.to_datetime(df['extract_date'])

        sns.set_theme(style="whitegrid")

        g = sns.catplot(
            data=df, kind="bar",
            x="extract_date", y="quantity", hue="artifact_type",
            errorbar="sd", palette="dark", alpha=.6, height=6
        )
        g.despine(left=True)
        g.set_axis_labels("", "Quantity")
        g.legend.set_title("Type of Artifacts")
        plt.xticks(rotation=90)
        st.pyplot(plt)
        df.rename(columns={'extract_date': 'Date Extract','quantity':'Quantity', 'artifact_type': 'Artifact Type'}, inplace=True)
        return df
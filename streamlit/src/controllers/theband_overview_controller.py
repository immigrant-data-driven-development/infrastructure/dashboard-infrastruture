import pandas as pd
import streamlit as st
import seaborn as sns
import matplotlib.pyplot as plt

from repository.theband.theband_repository import TheBandRepository

""" Zeppelin Page`s Controller  """
class TheBandOverviewController():
    
    def __init__(self):
        self.repository = TheBandRepository()
    
    def development_process(self):
        self.__average_waiting_time_user_story_until_initiated()
        self.__average_waiting_time_user_story_to_done()
        self.__average_waiting_delivery_time_user_story()
        self.__amount_rework_user_story()

    def __average_waiting_time_user_story_until_initiated(self):
        st.write("What is the average waiting time for a user story until its development is initiated?")
        
        df = self.repository.waiting_time()
        df.rename(columns={'average_waiting_day':'Average Waiting Day','project': 'Project'}, inplace=True)
        st.dataframe(df, use_container_width=True)
       
    def __average_waiting_time_user_story_to_done(self):
        st.write("What is the average development time for a user story (from the beginning of its development until it is successfully completed)?")
    
    def __average_waiting_delivery_time_user_story(self):
        st.write("What is the average delivery time for a user story (from its creation)?")
    
    def __amount_rework_user_story(self):
        st.write("What is the average amount of rework during the development of a product version?")
    
    def planning_project(self):
        self.__average_completion_time_item_sprint()
        self.__average_rate_time_item_planned_not_completed_sprint()
        self.__average_rate_time_item_planned_started_not_completed_sprint()
        
    def __average_completion_time_item_sprint(self):
        st.write("What is the average completion time successfully complete a backlog item in a sprint?") 
    
    def __average_rate_time_item_planned_not_completed_sprint(self):
        st.write("What is the average rate of items planned for the sprint that are not completed in a sprint?") 
    
    def __average_rate_time_item_planned_started_not_completed_sprint(self):
        st.write("What is the average rate of planned items for the sprint that are started but not completed in a sprint?")

    def quality_artifact(self):
        self.__most_code_smells()
        self.__code_smells_by_developer()
        self.__code_coverage_test_rate()
        self.__how_much_artifact_failed_automated_test()

    def __most_code_smells(self):
        st.write("What are most common code smells that are present in the artifacts generated by development team?") 
    
    def __code_smells_by_developer(self):
        st.write("What are the code smells that are present in the artifacts generated by each team member?")

    def __code_coverage_test_rate(self):
        st.write("What is the code coverage test rate?")

    def __how_much_artifact_failed_automated_test(self):
        st.write("How much of the artifact that failed in automated test?")

    def scopes(self):
        self.__user_stories_in_product_backlog()
        self.__growth_rate_product_backlog_project()
        self.__start_end_date_project()
        self.__team_member_more_than_project()

    def __user_stories_in_product_backlog(self):
        st.write ("What user stories are in the product backlog?")

        df = self.repository.product_backlog()

        df.rename(columns={'project':'Project','userstorytype': 'User Story Type','userstory': 'User Story',}, inplace=True)
        

        st.dataframe(df, use_container_width=True)

    def __growth_rate_product_backlog_project(self):
        st.write("What is the growth rate of the product backlog during the project?") 
        
        self.__growth_rate_product_backlog_project_without_accumulate()
        self.__growth_rate_product_backlog_project_acummulate()
        self.__growth_rate_project_view()

    def __growth_rate_project_view(self):
        df = self.repository.growth_rate_project()

        df['event_date'] = pd.to_datetime(df['event_date'])
        
        #sns.set_theme(style="darkgrid")
        
        df.rename(columns={'project':'Project','userstorytype': 'User Story Type','event_date': 'Created Date','growth_rate': 'Growth Rate'}, inplace=True)
        
        #sns.lineplot(x="Created Date", y="Growth Rate",hue="Project", style="User Story Type", data=df)
        
        #plt.xticks(rotation=90)
        #st.pyplot(plt)
        st.dataframe(df, use_container_width=True)

        return df
    
    def __growth_rate_product_backlog_project_without_accumulate(self):

        df = self.repository.growth_rate_product_backlog()

        df['event_date'] = pd.to_datetime(df['event_date'])
        
        #sns.set_theme(style="darkgrid")
        
        df.rename(columns={'project':'Project','userstorytype': 'User Story Type','event_date': 'Created Date','qtd': 'EPIC/User Story'}, inplace=True)
        
        #sns.lineplot(x="Created Date", y="EPIC/User Story",hue="Project", style="User Story Type", data=df)
        
        #plt.xticks(rotation=90)
        #st.pyplot(plt)
        st.dataframe(df, use_container_width=True)

        return df

    def __growth_rate_product_backlog_project_acummulate(self):

        dfc = self.repository.growth_rate_product_backlog_acummulate()

        dfc['event_date'] = pd.to_datetime(dfc['event_date'])
        
        sns.set_theme(style="darkgrid")
        
        dfc.rename(columns={'project':'Project','event_date': 'Created Date','accumulated_sum': 'EPIC/User Story'}, inplace=True)
        
        sns.lineplot(x="Created Date", y="EPIC/User Story", style="Project", data=dfc)
        
        plt.xticks(rotation=90)
        
        st.pyplot(plt)

        st.dataframe(dfc, use_container_width=True)

        return dfc

    def __start_end_date_project(self):
        st.write("What is the start and end date of the project?")

        df = self.repository.project_start_end_date()

        df['start_date'] = pd.to_datetime(df['start_date'])

        df['end_date'] = pd.to_datetime(df['end_date'])

        df.rename(columns={'name':'Project','start_date': 'Start Date','end_date': 'End Date'}, inplace=True)    
    
        st.dataframe(df, use_container_width=True)

    def __team_member_more_than_project(self):
        st.write("Which team members are on more than one project?")
    
    def quality_process(self):
        self.__which_project_are_using_ci_cd()
        self.__what_pipelone_rate_ci_completed()
        self.__what_pipelone_rate_cd_cde_completed()

    def __which_project_are_using_ci_cd(self):
        st.write("Which projects are using CI and CD concepts? (e.g., with automatic testing, automatic delivery, and automatic deployment)")
    
    def __what_pipelone_rate_ci_completed(self):
        st.write("What is the pipeline rate of continuous integration that are successfully completed?")
    
    def __what_pipelone_rate_cd_cde_completed(self):
        st.write("What is the pipeline rate of continuous delivery and continuous deployment that are successfully completed?")